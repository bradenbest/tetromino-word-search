# Tetromino Word Search

It's a word search generator, but the twist is that the words are in the shape of tetrominoes. Which actually makes this
surprisingly difficult even though the words are all four letters long.

To run, just run make and then run the program

    $ cd src/
    $ make
    $ ./tws
    tetromino word search v1.X
    Type 'tws -h' for help

    H X G N Y A N O U Z Z V S Q X D S C F P 
    F T A E S C W N R N M A D R B I R H C H 
    P U N X R O T G H R B I D N O X S I L O 
    W J I O G O G I T V P P H I I G I P J A 
    O B K S M E F V F D Q U M O C L U M K L 
    O D U H A E D H V Q F M G A T O D O Z M 
    Y J X O A H U I R L L M J Q G R T X E S 
    F G G O D W V L X O Z S I W N Q A X F A 
    N O X U K I N V T Z M E I C S B E H T W 
    B W P H D D N C B Z Y C H A P I S A O R 
    H A N W Q K R O C U G Y O H O R M P Q C 
    Z V H R Y Y Z O K A B Q P E C T C A R Q 
    P A S H I Q V O Q S M K M K M A O B U F 
    M P M M U D F K V Y E X X W A L S S Q V 
    G Q Y S P B A N Y I E N C I R E A L P Y 
    L P W A X Y A C Q I A J O B C S W Q K S 
    C I U P I I G W V L T T S I Q P Y L Y Q 
    S L L Y A O S W R I R Q Y A Z U N N Y Q 
    N L Q I G L M C E L I D T I C K I T E Y 
    U R R S H A X Q C E N E S C K K S X S A 
    Words:
        UNNY OOZY PILL HIND PACT 
        RIDE NEAT RILE WAXY DAMN 
        BANK KUDO LAWS SOBS CITY 
        ALMS WISE DUMP MALE PASH 
        WAFT SITE SOJA SPUD CHIS 
        NEEM NAYS MUMM CRAM RUNS 
        MEED BISE 
    Seed: 1712686024
