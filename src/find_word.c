/* Use this to find the offset where words are located, for updating the
 * swear list
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include "words.h"

struct {
    int reverse;
    int search_swears;
} options;

static void
usage(void)
{
    static char const *lines[] = {
        "find_word - locate words in words.h",
        "usage: find_word [options] <word> [word] ...",
        "",
        "options:",
        "  -l  list swear words and exit",
        "  -s  toggle between searching the normal dictionary and the swear",
        "      list (default: dictionary)",
        "  -r  toggle reverse lookup mode",
        "  -h  show this help and exit",
        "",
        "example: find_word fuc",
    };
    static size_t lines_len = sizeof lines / sizeof *lines;

    for (size_t i = 0; i < lines_len; ++i)
        puts(lines[i]);

    exit(1);
}

static int
find_word(char const *word)
{
    char buffer[4];
    size_t len = strlen(word);
    int nmatches = 0;
    size_t list_len = words_len;

    printf("Searching for words starting with '%s'\n", word);

    for (size_t i = 0; i < len; ++i) {
        if (!isalpha(word[i]))
            return 0;

        buffer[i] = toupper(word[i]);
    }

    if (options.search_swears)
        list_len = swear_words_len;

    for (size_t i = 0; i < list_len; ++i) {
        int selidx = i;

        if (options.search_swears)
            selidx = swear_words[i];

        if (strncmp(buffer, words[selidx], len) == 0) {
            if (++nmatches == 1) {
                puts("Match  Offset");
                puts("-----  ------");
            }

            printf("%5s  %u\n", words[selidx], selidx);
        }
    }

    return nmatches;
}

static void
show_word(size_t offset)
{
    if (offset < words_len)
        printf("%04lu '%s'\n", offset, words[offset]);
    else
        printf("find_word: offset %lu is out of bounds\n", offset);
}

static void
list_swears(void)
{
    puts("Swear words:");

    for (size_t i = 0; i < swear_words_len; ++i) {
        printf("  ");
        show_word(swear_words[i]);
    }

    exit(0);
}

static void
parse_flag(char const *flag)
{
    if (flag[0] == 'h')
        usage();

    if (flag[0] == 'r')
        options.reverse = !options.reverse;

    if (flag[0] == 'l')
        list_swears();

    if (flag[0] == 's')
        options.search_swears = !options.search_swears;
}

int
main(int argc, char **argv)
{
    (void) argc;

    if (argv[1] == NULL)
        usage();

    for (int i = 1; argv[i] != NULL; ++i) {
        char const *selarg = argv[i];

        if (selarg[0] == '-') {
            parse_flag(selarg + 1);
            continue;
        }

        if (options.reverse) {
            printf("Reverse lookup: ");
            show_word(atoi(selarg));
        }

        else
            printf("matches found: %u\n", find_word(selarg));
    }

    return 0;
}
