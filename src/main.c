/*
 * The idea: word search generator but the words are all 4 letters long and tetromino shaped
 * mechanism: choose random:
 * - word from words[]
 * - tetromino from tetrominoes[]
 * - grid position
 * - random orientation and then overwrite the grid positions with the letters of that word
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#include "words.h"

#define VERSION "1.4"
#define PROGNAME "tws"
#define NUMBER_OF_ATTEMPTS 10
#define WORD_WRAP_THRESHOLD 10
#define GRID_SIZE 20

#ifdef WASM
#    define WASM_EXPORT __attribute__((used))
#else
#    define WASM_EXPORT
#endif // WASM

#define TETROMINO_GET_X(t, n) \
    ( ((t)->x >> ((3 - (n)) * 2)) & 3 )

#define TETROMINO_GET_Y(t, n) \
    ( ((t)->y >> ((3 - (n)) * 2)) & 3 )

#define GRID_XY(x, y) \
    ( (y) * GRID_SIZE + (x) )

#define item_in_list_simple(haystack, needle, compare) \
    (item_in_list)((haystack), haystack##_len, sizeof *(haystack), &(needle), (compare))


/* 4 coordinate pairs stuffed in 2 bits each , see TETROMINO_GET_* macros and ../other/tetromino-tool.js */
struct tetromino {
    unsigned char x;
    unsigned char y;
};

typedef int (*comparefn)(void const *a, void const *b);

static int comparefn_int_eq (void const *a, void const *b);
static int comparefn_ptr_eq (void const *a, void const *b);
static int comparefn_str4   (void const *a, void const *b);

static int          min                     (int a, int b);
static int          item_in_list            (void const *haystack, size_t len, size_t size, void const *needle, comparefn compare);
static int          list_uniq               (void const *list, size_t len, size_t size, comparefn compare);
static int          irand                   (void);
static void         sirand                  (unsigned int seed);
static char const * get_unique_random_word  (void);
static void         word_list_print         (char const *label, char const **list, size_t len);
static void         grid_init               (void);
static void         grid_randomize          (void);
static int          grid_try_apply_word     (char const *word, int x, int y, struct tetromino const *t);
static int          grid_apply_word         (char const *word);
static void         grid_print              (void);
static size_t       check_grid_at           (int x, int y, int *stack, size_t stack_len, char const **bonus_words, size_t bonus_words_len);
static void         print_bonus_words       (size_t word_limit);
static void         parse_args              (char **args);
static void         usage                   (void);
int                 tws_main                (int argc, char **argv);
int                 main                    (int argc, char **argv);

/* some duplicates are necessary to get more balanced tetromino
 * distribution. I could just create a tetromino enum to choose between
 * the types and then choose randomly from that type, but this is simpler
 */
static struct tetromino const tetrominoes[] = {
    // S-type (4 base, 4 dupes))
    {0x16, 0x50}, {0x94, 0x05}, // S 0
    {0x05, 0x16}, {0x50, 0x94}, // S 90
    {0x16, 0x05}, {0x94, 0x50}, // Z 0
    {0x50, 0x16}, {0x05, 0x94}, // Z 90
    {0x16, 0x50}, {0x94, 0x05}, // S 0
    {0x05, 0x16}, {0x50, 0x94}, // S 90
    {0x16, 0x05}, {0x94, 0x50}, // Z 0
    {0x50, 0x16}, {0x05, 0x94}, // Z 90

    // I-type (2 base, 6 dupes)
    {0x00, 0x1B}, {0x00, 0xE4}, // I 0
    {0x1B, 0x00}, {0xE4, 0x00}, // I 90
    {0x00, 0x1B}, {0x00, 0xE4}, // I 0
    {0x1B, 0x00}, {0xE4, 0x00}, // I 90
    {0x00, 0x1B}, {0x00, 0xE4}, // I 0
    {0x1B, 0x00}, {0xE4, 0x00}, // I 90
    {0x00, 0x1B}, {0x00, 0xE4}, // I 0
    {0x1B, 0x00}, {0xE4, 0x00}, // I 90

    // O-type (4 base, 4 dupes)
    {0x14, 0x05}, {0x05, 0x14}, // O (upper left start)
    {0x50, 0x14}, {0x41, 0x05}, // O (upper right start)
    {0x05, 0x41}, {0x14, 0x50}, // O (lower left start)
    {0x41, 0x50}, {0x50, 0x41}, // O (lower right start)
    {0x14, 0x05}, {0x05, 0x14}, // O (upper left start)
    {0x50, 0x14}, {0x41, 0x05}, // O (upper right start)
    {0x05, 0x41}, {0x14, 0x50}, // O (lower left start)
    {0x41, 0x50}, {0x50, 0x41}, // O (lower right start)

    // L-type (8 base, 0 dupes)
    {0x54, 0x1A}, {0x15, 0xA4}, // J 0
    {0x06, 0x15}, {0x90, 0x54}, // J 90
    {0x01, 0x90}, {0x40, 0x06}, // J 180
    {0x1A, 0x01}, {0xA4, 0x40}, // J 270
    {0x01, 0x1A}, {0x40, 0xA4}, // L 0
    {0x06, 0x40}, {0x90, 0x01}, // L 90
    {0x15, 0x06}, {0x54, 0x90}, // L 180
    {0x1A, 0x54}, {0xA4, 0x15}, // L 270
};
static size_t const tetrominoes_len = sizeof tetrominoes / sizeof *tetrominoes;

static struct {
    int help;
    int easy_mode;
    int seed_override;
    int bonus_words;
    int swear_words;
    int word_limit;
} options;

static unsigned char grid[GRID_SIZE * GRID_SIZE];

static char const *used_words[100];
static size_t used_words_len;

static int random_seed_initial;

static unsigned int irand_seed;

/* comparefn_*: comparison functions for sorting/searching (the type
 * matches qsort's callback, for instance)
 *
 * 1. qsort actually sends offsets into the array, not elements from it
 */
static int
comparefn_int_eq(void const *a, void const *b)
{
    return *(int const *)a == *(int const *)b;
}

static int
comparefn_ptr_eq(void const *a, void const *b)
{
    return *(char const **)a == *(char const **)b;
}

static int
comparefn_str4(void const *a, void const *b)
{
    return strncmp(*(char const * const *)a, *(char const * const *)b, 4); // 1
}

/* min: returns the smaller value
 *
 * This could be implemented as a macro, but then parse_args would call
 * atoi twice
 */
static int
min(int a, int b)
{
    return a < b ? a : b;
}

/* item_in_list: returns position of item in list or -1
 *
 * Note that in instances where this is used with comparefn_ptr_eq on a
 * word pulled from words[], this comparison is intended, since the
 * address is the same and thus == works. For dynamically generated
 * strings such as stack_word[] in check_grid_at, strncmp is necessary.
 *
 * See also: item_in_list_simple
 */
static int
item_in_list(void const *haystack, size_t len, size_t size, void const *needle, comparefn compare)
{
    for (size_t i = 0; i < len; ++i)
        if (compare(haystack + (i * size), needle))
            return i;

    return -1;
}

/* list_uniq: check if list contains all unique items
 *
 * Returns 1 to indicate list is all unique, 0 otherwise
 */
static int
list_uniq(void const *list, size_t len, size_t size, comparefn compare)
{
    for (size_t i = 0; i < len; ++i)
        for (size_t j = i + 1; j < len; ++j)
            if (compare(list + (i * size), list + (j * size)))
                return 0;

    return 1;
}

/* irand = returns a random int
 *
 * libc rand is fine, but it's not guaranteed to be implemented a
 * particular way. The reason for this custom implementation is to make
 * the puzzle generation consistent across platforms. WASM is the major
 * outlier here.
 *
 * An LCG should suffice.
 *
 * a = 1 + 4 x 3 x 5 x 7 x 11 x 13 x 17 x 19 x 23
 */
static int
irand(void)
{
    irand_seed = ((irand_seed * 446185741) + 12345) & 0x7FFFFFFF;
    return irand_seed;
}

/* sirand - seeds irand
 */
static void
sirand(unsigned int seed)
{
    irand_seed = seed;
}

/* get_unique_random_word: returns unique pointer from words[]
 *
 * also adds said word to used word list, which pulls double duty as the
 * thing this function checks to see if a word is unique and as the word
 * list displayed to the user in the puzzle.
 *
 * 1. Returns NULL if 100 words are in the list. 100 words is the limit
 *    because the default grid is 20x20 (400 long), and 100 words x 4
 *    letters per word = 400 letters.
 *
 * 2. This should be unreachable
 */
static char const *
get_unique_random_word(void)
{
    char const *candidate;

    if (used_words_len == 100)
        return NULL; // 1

    while (1) {
        int offset = irand() % words_len;
        candidate = words[offset];

        if (item_in_list_simple(used_words, candidate, comparefn_ptr_eq) != -1)
            continue;

        if (options.swear_words && item_in_list_simple(swear_words, offset, comparefn_int_eq) != -1)
            continue;

        used_words[used_words_len++] = candidate;
        return candidate;
    }

    return NULL; // 2
}

/* word_list_print: sorts and prints word list
 *
 * a "word list" in this project is an array of pointers borrowed from
 * words[] (words.h). They should not be confused with buffers or
 * dynamic strings, as some assumptions apply in many cases:
 *
 * - words all have a strlen() of 4
 * - words can be compared with ==
 * - words are read-only
 *
 * For this function, the distinction doesn't matter a great deal, as the
 * only thing that breaking these assumptions will screw up is the column
 * alignment in the printout.
 */
static void
word_list_print(char const *label, char const **list, size_t len)
{
    qsort(list, len, sizeof *list, comparefn_str4);
    printf("%s (%lu): ", label, len);

    if (len == 0) {
        puts("List is empty");
        return;
    }

    for (size_t i = 0; i < len; ++i) {
        if (i % WORD_WRAP_THRESHOLD == 0)
            printf("\n    ");

        printf("%s ", list[i]);
    }

    putchar('\n');
}


/* grid_init: initializes the grid with dots
 */
static void
grid_init(void)
{
    for (int i = 0; i < GRID_SIZE * GRID_SIZE; ++i)
        grid[i] = '.';
}

/* grid_randomize: replaces dots in grid with random letters
 */
static void
grid_randomize(void)
{
    for (int i = 0; i < GRID_SIZE * GRID_SIZE; ++i)
        if (grid[i] == '.')
            grid[i] = 'A' + (irand() % 26);
}

/* grid_try_apply_word: attempts to place a word on the grid
 *
 * Returns 1 or 0 to indicate success/failure. If a letter fails to be
 * placed, the remnants of the words are left in place as "junk".
 *
 * 1. The space must be in-bounds. Having words pacman-warp around the
 *    edge would be unfair.
 *
 * 2. The space must be blank OR already match the letter being placed
 *    there
 */
static int
grid_try_apply_word(char const *word, int x, int y, struct tetromino const *t)
{
    for (int i = 0; i < 4; ++i) {
        int gridx = TETROMINO_GET_X(t, i) + x;
        int gridy = TETROMINO_GET_Y(t, i) + y;
        int offset = GRID_XY(gridx, gridy);

        if (gridx < 0 || gridx > (GRID_SIZE - 1) || gridy < 0 || gridy > (GRID_SIZE - 1)) // 1
            return 0;

        if (grid[offset] != '.' && grid[offset] != word[i]) // 2
            return 0;


        grid[offset] = word[i];
    }

    return 1;
}

/* grid_apply_word: applies a word to the grid
 *
 * Returns 1 or 0 to indicate success/failure. On failure, puzzle
 * generation is terminated.
 *
 * Failure happens on any the following conditions:
 * - word is NULL (this can only mean the word limit has been reached)
 * - word failed to be placed NUMBER_OF_ATTEMPTS times
 *
 * In either case, the grid is sufficiently full.
 */
static int
grid_apply_word(char const *word)
{
    if (word == NULL)
        return 0;

    for (int i = 0; i < NUMBER_OF_ATTEMPTS; ++i) {
        int x = irand() % GRID_SIZE;
        int y = irand() % GRID_SIZE;
        struct tetromino const * t = tetrominoes + (irand() % tetrominoes_len);

        if (grid_try_apply_word(word, x, y, t))
            return 1;
    }

    return 0;
}

/* grid_print: prints the grid, word list, and seed
 */
static void
grid_print(void)
{
    for (int y = 0; y < GRID_SIZE; ++y) {
        for (int x = 0; x < GRID_SIZE; ++x)
            printf("%c ", grid[GRID_XY(x, y)]);

        puts("");
    }

    word_list_print("Words", used_words, used_words_len);

    if (options.bonus_words)
        print_bonus_words(options.word_limit);

    else if (used_words_len < (size_t)options.word_limit)
        print_bonus_words((size_t)options.word_limit - used_words_len);

    printf("Seed: %i\n", random_seed_initial);
}

/* check_grid_at: checks location on the grid for possible words
 *
 * Returns new length of bonus word list
 *
 * Given an (x, y) location, a stack, and a list of found bonus words,
 * this function recursively crawls all possible spots and checks each
 * completed word against the dictionary, used word list and bonus word
 * list. When a word is found and is able to be added to the bonus word
 * list, the word is added and the new length of the list is returned.
 * The effect bubbles up until all words starting from that spot are
 * found.
 *
 * 1. The stack holds grid offsets, so this buffer is used to hold the
 *    string once the stack is full
 *
 * 2. Bounds checks. We don't want x,y going off-grid and the limit for
 *    the bonus word list is 1000
 *
 * 3. Take easy mode into account; ignore dots.
 *
 * 4. if the stack contains a duplicate offset, then we've backtracked.
 *    Tetrominoes do not overlap with themselves, so this is invalid.
 *
 * 5. For all cases where the stack isn't full, recurse into the four
 *    adjacent spots and return
 *
 * 6. Optimization. There's no point in fully strncmping words that don't
 *    match on the first letter, and once we're past that first letter in
 *    the dictionary, we can stop looking.
 *
 * 7. we *specifically* want to use selword here, not the buffer. selword
 *    has the constant address.
 *
 * 8. Once we've found a match, we're done. Recall that if we're here,
 *    the stack is full
 *
 * 9. Per v1.3's swear word omission feature
 */
static size_t
check_grid_at(int x, int y, int *stack, size_t stack_len, char const **bonus_words, size_t bonus_words_len)
{
    int offset = GRID_XY(x, y);
    char stack_word[4] = ""; // 1

    if (bonus_words_len == 1000) // 2
        return bonus_words_len;

    if (x < 0 || x > (GRID_SIZE - 1) || y < 0 || y > (GRID_SIZE - 1)) // 2
        return bonus_words_len;

    if (grid[offset] == '.') // 3
        return bonus_words_len;

    stack[stack_len++] = offset;

    if (!list_uniq(stack, stack_len, sizeof *stack, comparefn_int_eq)) // 4
        return bonus_words_len;

    if (stack_len < 4) { // 5
        bonus_words_len = check_grid_at(x + 1, y + 0, stack, stack_len, bonus_words, bonus_words_len);
        bonus_words_len = check_grid_at(x - 1, y + 0, stack, stack_len, bonus_words, bonus_words_len);
        bonus_words_len = check_grid_at(x + 0, y + 1, stack, stack_len, bonus_words, bonus_words_len);
        bonus_words_len = check_grid_at(x + 0, y - 1, stack, stack_len, bonus_words, bonus_words_len);
        return bonus_words_len;
    }

    for (int i = 0; i < 4; ++i)
        stack_word[i] = grid[stack[i]]; // 1

    for (size_t i = 0; i < words_len; ++i) {
        char const *selword = words[i];

        if (selword[0] < grid[stack[0]]) // 6
            continue;

        if (selword[0] > grid[stack[0]]) // 6
            break;

        if (strncmp(selword, stack_word, 4) == 0) {
            int words_offset = item_in_list_simple(words, selword, comparefn_ptr_eq);       // 7
            int used_offset  = item_in_list_simple(used_words, selword, comparefn_ptr_eq);  // 7
            int bonus_offset = item_in_list_simple(bonus_words, selword, comparefn_ptr_eq); // 7

            if (options.swear_words && item_in_list_simple(swear_words, words_offset, comparefn_int_eq) != -1)
                continue; // 9

            if (used_offset != -1 || bonus_offset != -1)
                continue;

            bonus_words[bonus_words_len++] = selword; // 7
            break; // 8
        }
    }

    return bonus_words_len;
}

/* print_bonus_words: scans grid for extra words and prints them
 *
 * This uses a recursive brute force search, so due to performance
 * implications, it's locked behind the -b switch
 *
 * 1. It should be noted that word_list_print sorts the list before
 *    printing it. This was originally designed as an anti-cheese
 *    measure, but since the code to shuffle or sort this list is no
 *    longer IN this function, it's being mentioned here. About the
 *    anti-cheese: Since the bonus list is generated from the grid
 *    (rather than the other way around like with used_words[]), it can
 *    be cheesed by just scanning the grid from top left to bottom
 *    right. Sorting or shuffling the list prevents this.
 *
 * 2. word limits were added in v1.4.
 */
static void
print_bonus_words(size_t word_limit)
{
    static char const *bonus_words[1000];
    size_t bonus_words_len = 0;
    int offset_stack[4];

    for (int y = 0; y < GRID_SIZE; ++y) {
        for (int x = 0; x < GRID_SIZE; ++x) {
            bonus_words_len = check_grid_at(x, y, offset_stack, 0, bonus_words, bonus_words_len);

            if (word_limit > 0 && bonus_words_len >= word_limit) { // 2
                bonus_words_len = word_limit;
                break;
            }
        }
    }

    if (bonus_words_len == 0)
        return;

    word_list_print("Bonus Words", bonus_words, bonus_words_len); // 1
}

/* parse_args: parses argv and sets any options the user calls for
 */
static void
parse_args(char **args)
{
    for (int i = 1; args[i] != NULL; ++i) {
        if (args[i][0] == '-') {
            if (args[i][1] == 'h')
                options.help = 1;

            if (args[i][1] == 'e')
                options.easy_mode = 1;

            if (args[i][1] == 'b')
                options.bonus_words = 1;

            if (args[i][1] == 's')
                options.swear_words = 1;

            if (args[i][1] == 'l') {
                options.word_limit = min(100, atoi(args[i + 1]));
                i += 1;
            }
        }

        else if (isdigit(args[i][0])) {
            options.seed_override = 1;
            random_seed_initial = atoi(args[i]);
        }
    }
}

/* usage: prints usage help and how to play instructions, then exits
 */
static void
usage(void)
{
    static char const *lines[] = {
#ifdef WASM
        "tetromino word search v" VERSION " (WebAssembly edition) by Braden Best",
        "Usage: Set the options above and click 'Generate Puzzle'",
        "",
#else
        "tetromino word search v" VERSION " by Braden Best",
        "Usage: tws [options] [seed]",
        "",
        "options:",
        "  -h          show this help",
        "  -e          generate an easier puzzle (this alters the seed",
        "              as an anti-cheese measure)",
        "  -b          show bonus words (it's a lot)",
        "  -s          omit swear words",
        "  -l <limit>  set word limit to <limit>.",
        "",
        "seed: enter a number to override the seed",
        "",
#endif // WASM
        "how to play:",
        "In this variant of word search, the words are all four letters long and",
        "are tetromino-shaped. Don't let the length of the words deceive you,",
        "this game is surprisingly difficult!",
        "",
        "Words must flow from end to end, and they may overlap. There are no",
        "diagonal words. There are also no T-shaped words, as the T tetromino",
        "breaks the flow rule.",
        "",
        "easy mode:",
        "This is the same as normal mode, except the grid isn't filled with",
        "random letters after the puzzle is generated.",
        "",
        "bonus words:",
        "The program will scan the puzzle for extra words and display them below",
        "the word list the puzzle was generated from. Works best with easy mode",
        "because on normal mode it easily finds hundreds of words",
        "",
        "word limit:",
        "If the number of words generated is less than the limit, words will",
        "be pulled from bonus words to meet it. Default = 0. Max = 100.",
    };
    static size_t const lines_len = sizeof lines / sizeof *lines;

    for (size_t i = 0; i < lines_len; ++i)
        puts(lines[i]);
}

/* tws_main: self-explanatory
 *
 * 1. We use a placeholder string here rather than NULL for reasons that
 *    should be obvious
 *
 * 2. This is an anti-cheese measure. The seed is unchanged, but the RNG
 *    state is manipulated so that a completely different puzzle will be
 *    generated from that of hard mode with the same seed.
 *
 * 3. The loop is terminated by either running out of words or failing
 *    to place a word. See grid_apply_word and get_unique_random_word.
 *
 * 4. It's necessary to decrement this variable so that the failed word
 *    is excluded from the word list. We don't want to send the player on
 *    a snipe hunt for a word that doesn't exist on the grid.
 *
 * 5. The only real difference between easy and hard mode is that easy
 *    mode keeps the dots.
 *
 * 6. word limit option added in v1.4
 */
WASM_EXPORT int
tws_main(int argc, char **argv)
{
    (void) argc;
    char const *word = "placeholder"; // 1

    parse_args(argv);

    if (options.help) {
        usage();
        return 1;
    }

    if (!options.seed_override)
        random_seed_initial = time(NULL);

    sirand(random_seed_initial);
    grid_init();

    if (options.easy_mode) {
        sirand((random_seed_initial >> 1) + 12345); // 2
        (void) irand();
    }

    while (word != NULL) { // 3
        word = get_unique_random_word();

        if (!grid_apply_word(word)) {
            --used_words_len; // 4
            break; // 3
        }

        if (options.word_limit > 0 && used_words_len == (size_t)options.word_limit)
            break; // 6
    }

    if (!options.easy_mode)
        grid_randomize(); // 5

    puts("tetromino word search v" VERSION);
    puts("Type '" PROGNAME " -h' for help");
    putchar('\n');
    grid_print();
    return 0;
}

#ifdef WASM

/* call_main: calls main with arguments obtained from Module.ccall.
 *
 * Module.ccall seems to be unable to pass strings, so in order to work
 * around this, the arguments have to all be taken.
 *
 * 1. These have to be set every time the program runs.
 */
WASM_EXPORT void
call_main(int seed, int word_limit, int flags)
{
    static int const FLAG_HELP = 0x1;
    static int const FLAG_EASY_MODE = 0x2;
    static int const FLAG_SEED_OVERRIDE = 0x4;
    static int const FLAG_BONUS_WORDS = 0x8;
    static int const FLAG_SWEAR_WORDS = 0x10;

    static char *argv[2] = {"tws", NULL};

    // 1
    options.help = !!(flags & FLAG_HELP);
    options.easy_mode = !!(flags & FLAG_EASY_MODE);
    options.seed_override = !!(flags & FLAG_SEED_OVERRIDE);
    options.bonus_words = !!(flags & FLAG_BONUS_WORDS);
    options.swear_words = !!(flags & FLAG_SWEAR_WORDS);
    options.word_limit = min(100, word_limit);
    used_words_len = 0;

    if (options.seed_override)
        random_seed_initial = seed;

    (void) tws_main(0, argv);
}

int
main(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    return 0;
}

#else

int
main(int argc, char **argv)
{
    return tws_main(argc, argv);
}

#endif // WASM
