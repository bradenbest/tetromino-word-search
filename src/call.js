const FLAG_HELP = 0x1;
const FLAG_EASY_MODE = 0x2;
const FLAG_SEED_OVERRIDE = 0x4;
const FLAG_BONUS_WORDS = 0x8;
const FLAG_SWEAR_WORDS = 0x10;

const main_args = _ => ({
    seed:       0,
    word_limit: 0,
    flags:      0,
});
const easymode_box    = document.querySelector(".startform.easymode");
const bonuswords_box  = document.querySelector(".startform.bonuswords");
const swearwords_box  = document.querySelector(".startform.swearwords");
const seed_input      = document.querySelector(".startform.seed");
const wordlimit_input = document.querySelector(".startform.wordlimit");
const generate_btn    = document.querySelector(".startform.generate");
const help_btn        = document.querySelector(".startform.help");
const textarea        = document.querySelector("textarea#output");

generate_btn.addEventListener("click", function(ev){
    let args = main_args();

    if (easymode_box.checked)
        args.flags |= FLAG_EASY_MODE;

    if (seed_input.value !== "")
        args.flags |= FLAG_SEED_OVERRIDE;

    if (bonuswords_box.checked)
        args.flags |= FLAG_BONUS_WORDS;

    if (swearwords_box.checked)
        args.flags |= FLAG_SWEAR_WORDS;

    args.seed = (seed_input.value)|0;
    args.word_limit = (wordlimit_input.value)|0;

    textarea.value = "";
    main(args);
});

help_btn.addEventListener("click", function(ev){
    let args = main_args();

    args.flags |= FLAG_HELP;
    textarea.value = "";
    main(args);
});

function main(args){
    Module.ccall(
        "call_main",
        null,
        ["int", "int", "int"],
        [args.seed, args.word_limit, args.flags]
    );
}
