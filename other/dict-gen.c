// Generate word list from dictionary.txt of all the 4 letter words
#include <stdio.h>
#include <string.h>

static size_t
read_line(char *buffer, size_t bufsz)
{
    int ch;
    size_t nread = 0;

    while ((ch = getchar()) != EOF && ch != '\n' && nread < bufsz)
        buffer[nread++] = ch;

    return nread;
}

int
main(void)
{
    static char linebuf[4096];
    size_t linebuflen;
    size_t nwords = 0;

    printf("static char const *words[] = {\n");

    while ((linebuflen = read_line(linebuf, 4096)) > 0) {
        if (linebuflen == 4) {
            printf("    \"%.4s\",\n", linebuf);
            ++nwords;
        }
    }

    printf("};\nstatic size_t const words_len = %lu;\n", nwords);

    return 0;
}
