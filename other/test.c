// testing functions
static void
tetromino_print(struct tetromino const *t)
{
    char grid[16] = "................";

    char const *word = words[rand() % words_len];

    for (int i = 0; i < 4; ++i)
        grid[TETROMINO_GET_Y(t, i) * 4 + TETROMINO_GET_X(t, i)] = word[i];

    for (int i = 0; i < 16; i += 4) {
        for (int j = 0; j < 4; ++j)
            printf("%c ", grid[i + j]);

        putchar('\n');
    }
}

static void
test_tetrominoes(void)
{
    for(size_t i = 0; i < tetrominoes_len; ++i){
        tetromino_print(tetrominoes + i);
        puts("");
    }
}

static void
test_irand(void)
{
    int period = 1;
    unsigned int iseed = irand();

    irand();
    printf("testing irand period...\n");

    while (iseed != irand_seed){
        irand();
        ++period;
    }

    printf("period = %u\n", period);
    printf("sample values:\n");
    printf("%u\n%u\n%u\n", irand(), irand(), irand());
}
