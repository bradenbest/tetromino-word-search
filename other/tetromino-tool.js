let accumulator_x = 0;
let accumulator_y = 0;

let buttonarray = [
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
    document.createElement("button"),
];
let text = document.createElement("textarea");
let rsbtn = document.createElement("button");

rsbtn.innerHTML = "Reset";
rsbtn.addEventListener("click", rsbtn_click);

buttonarray.forEach((btn, idx) => {
    if (idx % 4 == 0 && idx > 0)
        document.body.appendChild(document.createElement("br"));

    document.body.appendChild(btn);
    btn.value = idx;
    btn.style.width = "50px";
    btn.style.height = "50px";

    btn.addEventListener("click", btn_click);
})

function btn_click(ev){
    let idx = ev.target.value | 0;
    let y = Math.floor(idx / 4);
    let x = idx % 4;
    ev.target.style.background = "#0a0";

    accumulator_x = (accumulator_x << 2) | x;
    accumulator_y = (accumulator_y << 2) | y;
    text.value = `0x${accumulator_x.toString(16)}, 0x${accumulator_y.toString(16)}`;
}

function rsbtn_click(ev){
    accumulator_x = 0;
    accumulator_y = 0;
    buttonarray.forEach(btn => {
        btn.style.background = "white";
    });
}

document.body.appendChild(document.createElement("br"));
document.body.appendChild(text);
document.body.appendChild(document.createElement("br"));
document.body.appendChild(rsbtn);

